# Weather app

Created by **Fabio Carbone**
Frontend developer at PED

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Live preview

You can check a live preview here: https://zealous-knuth-b2ead9.netlify.app/

## Install the app

In the project directory, run:

`npm install`

to install all the required dependencies.

### Run the app

In the project directory, run:

`npm start`

to run the app in the development mode.
Then open [http://localhost:3000](http://localhost:3000) to view it in the browser.