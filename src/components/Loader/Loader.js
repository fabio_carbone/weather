//Image
import loadingGif from '../../images/loading.gif';

//Style
import './Loader.css';

function Loader(){
    return (
        <div className="Loader">
            <img src={loadingGif} alt="loading" />
        </div>
    );
}

export default Loader;