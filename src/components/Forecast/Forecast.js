import { useEffect } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';

//Internationalization
import { useTranslation } from 'react-i18next';

//Redux
import openWeatherMapRequestThunk from '../../redux/thunks';
import { setPosition } from '../../redux/actions';

//Components
import SingleForecast from './SingleForecast/SingleForecast';
import Loader from '../Loader/Loader';

//Style
import './Forecast.css';

function Forecast() {

    const dispatch = useDispatch();
    const position = useSelector(state => state.position);
    const forecasts = useSelector(state => state.forecasts);
    const { t, i18n } = useTranslation();

    useEffect(() => {
        //Ask for use browser position
        if (!position) {
            navigator.geolocation.getCurrentPosition((position) => {
                dispatch(setPosition(position));
            });
        }

        //Every hour update the weather data
        const updateInterval = setInterval(() => {
            if (position) {
                dispatch(openWeatherMapRequestThunk(position.coords.latitude, position.coords.longitude, 'forecast', i18n.language));
            }
        }, [3600000]);

        return () => clearInterval(updateInterval);

    }, []);

    //Ask for forecast data, when position is set
    useEffect(() => {
        if (position) {
            dispatch(openWeatherMapRequestThunk(position.coords.latitude, position.coords.longitude, 'forecast', i18n.language));
        }
    }, [position]);

    const forecastsDOM = forecasts.map(forecast => (
        <SingleForecast key={forecast.dt} forecast={forecast}/>
    ));

    return (
        <div className="Forecast">
            
            <h1>{t('forecast')}</h1>

            {forecasts.length ?
                <div className="Forecast__all">
                    {forecastsDOM}
                </div>
                :
                <Loader />
            }

            <Link to="/"> {t('back to home')} </Link>
        </div>
    );
}

export default Forecast;