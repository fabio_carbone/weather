//Internationalization
import { useTranslation } from 'react-i18next';

//Images
import WeatherIcon from 'react-icons-weather';

//Style
import './SingleForecast.css';

function SingleForecast(props) {

    const { t } = useTranslation();

    const getFormattedTimestampFromEpoch = (epoch) => {
        var date = new Date(0);
        date.setUTCSeconds(epoch);
        var d = date.getDate();
        var m = date.getMonth() + 1; //Month from 0 to 11
        return (d <= 9 ? '0' + d : d) + '/' + (m <= 9 ? '0' + m : m)
    }

    const getHourFromEpoch = (epoch) => {
        var date = new Date(0);
        date.setUTCSeconds(epoch);
        return date.getHours();
    }

    return (
        <div className="SingleForecast">
            <div className="SingleForecast__icon">
                <WeatherIcon name="owm" iconId={props.forecast.weather[0].id.toString()} />
            </div>
            <div>
                <p>
                    {getFormattedTimestampFromEpoch(props.forecast.dt)} {t('at')} {getHourFromEpoch(props.forecast.dt)}
                    <br></br>
                    {props.forecast.weather[0].description}
                </p>
            </div>
        </div>
    );
}

export default SingleForecast;