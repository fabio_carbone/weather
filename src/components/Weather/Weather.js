import { useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { Link } from "react-router-dom";

//Internationalization
import { useTranslation } from 'react-i18next';

//Icons
import WeatherIcon from 'react-icons-weather';

//Redux
import openWeatherMapRequestThunk from '../../redux/thunks';
import { setPosition } from '../../redux/actions';

//Components
import Loader from '../Loader/Loader';

//Style
import './Weather.css';

function Weather() {

    const dispatch = useDispatch();
    const position = useSelector(state => state.position);
    const weather = useSelector(state => state.weather);
    const { t, i18n } = useTranslation();

    useEffect(() => {
        
        //Ask for use browser position
        if (!position) {
            navigator.geolocation.getCurrentPosition((position) => {
                dispatch(setPosition(position));
            });
        }

        //Every hour update the weather data
        const updateInterval = setInterval(() => {
            if (position) {
                dispatch(openWeatherMapRequestThunk(position.coords.latitude, position.coords.longitude, 'weather', i18n.language));
            }
        }, [3600000]);

        return () => clearInterval(updateInterval);

    }, []);

    //Ask for weather data, when position is set and when language change
    useEffect(() => {
        if (position) {
            dispatch(openWeatherMapRequestThunk(position.coords.latitude, position.coords.longitude, 'weather', i18n.language));
        }
    }, [position, i18n.language]);

    const addZero = (number) => {
        return number <= 9 ? '0' + number : number
    }

    //Date format
    const getTimestampFromEpoch = (epoch) => {
        var date = new Date(0);
        date.setUTCSeconds(epoch);
        var d = date.getDate();
        var M = date.getMonth() + 1;
        var y = date.getFullYear();
        var h = date.getHours();
        var m = date.getMinutes();
        var s = date.getSeconds();
        return (addZero(d)) + '-' + (addZero(M)) + '-' + y + ', ' + addZero(h) + ':' + addZero(m) + ':' + addZero(s);
    }

    return (
        <div className="Weather">
            {weather ?
                <div>
                    <div className="Weather__icon">
                        <WeatherIcon name="owm" iconId={weather.weather[0].id.toString()} />
                        <span>{weather.main.temp} °C</span>
                    </div>
                    <div className="Weather__description">
                        <h1>{t('today')} <b>{weather.name}</b> {t('there is')} <b>{weather.weather[0].description}</b></h1>
                        <p>{t('last update')} <span>{getTimestampFromEpoch(weather.dt)}</span></p>
                        <p><b>Powered by Synestesia</b></p>
                    </div>

                    <Link to="/forecast"> {t('go to forecast')} </Link>

                    <div className="Weather__language">
                        <p>{t('language')}</p>
                        <div>
                            <button className={(i18n.language === 'en') ? 'active' : ''} onClick={() => i18n.changeLanguage('en')}>EN</button>
                            <button className={(i18n.language === 'it') ? 'active' : ''} onClick={() => i18n.changeLanguage('it')}>IT</button>
                        </div>
                    </div>

                </div>
                :
                <Loader />
            }
        </div>
    );
}

export default Weather;