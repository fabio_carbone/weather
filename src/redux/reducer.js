const initialState = { 
    position: null, //the current user position
    weather: null, //the weather object, describes the current weather.
    forecasts: [] //list of forecast for the next 3 days
}

function reducer(state = initialState, action) {
    switch(action.type){
        case 'SET_POSITION':
            return {
                ...state,
                position: action.payload
            }
        case 'SET_WEATHER':
            return {
                ...state,
                weather: action.payload
            }
        case 'SET_FORECASTS':
            return {
                ...state,
                forecasts: action.payload
            }
        default:
            return state
    }
}

export {reducer}