import openWeatherMapRequest from '../services/api';
import { setWeather, setForecasts } from './actions';

/**
 * The thunk to manage asynchronously the weather request
 * @param  {Number} lan Langitude
 * @param  {Number} lon Longitude
 * @param  {String} lang language
 * @param  {String} type weather or forecast
 */
function openWeatherMapRequestThunk(lat, lon, type, lang) {
    return function (dispatch) {
        openWeatherMapRequest(lat, lon, type, lang)
            .then(response => response.json())
            .then(data => {
                if(type === 'weather'){
                    dispatch(setWeather(data));
                } else if (type === 'forecast') {
                    dispatch(setForecasts(data.list));
                }
            });
    };
}

export default openWeatherMapRequestThunk