/**
 * Action creator for position object
 * @param  {Object} position The position object
 * @return {Action} The action to set the position
 */
function setPosition(position) {
    return {
        type: 'SET_POSITION',
        payload: position
    };
}

/**
 * Action creator for weather object
 * @param  {Object} weather The weather object
 * @return {Action} The action to set the weather
 */
function setWeather(weather) {
    return {
        type: 'SET_WEATHER',
        payload: weather
    };
}

/**
 * Action creator for forecasts object
 * @param  {Object} forecasts The forecasts object
 * @return {Action} The action to set the forecasts
 */
function setForecasts(forecasts) {
    return {
        type: 'SET_FORECASTS',
        payload: forecasts
    };
}


export {setPosition, setWeather, setForecasts}