import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

//Redux
import { Provider } from 'react-redux';
import store from './redux/store';

//Components
import Weather from './components/Weather/Weather';
import Forecast from './components/Forecast/Forecast';

//Style
import './App.css';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Router>
          <Switch>
            <Route path="/" exact>
              <Weather />
            </Route>
            <Route path="/forecast" exact>
              <Forecast />
            </Route>
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
