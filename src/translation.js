const resources = {
    en: {
        translation: {
            "today": "Today in ",
            "there is": "there is",
            "last update": "Last update",
            "go to forecast": "Go to forecast",
            "forecast": "Forecast",
            "back to home": "Back to home",
            "at": "at",
            "language": "Language"
        }
    },
    it: {
        translation: {
            "today": "Oggi a ",
            "there is": "c'è",
            "last update": "Ultimo aggiornamento",
            "go to forecast": "Vai alle previsioni",
            "forecast": "Previsioni",
            "back to home": "Torna alla home",
            "at": "alle",
            "language": "Lingua"
        }
    }
};

export default resources