const apiKey = 'd64af34ad0a091d21aafd5d2802bbd13';
const baseUrl = 'https://api.openweathermap.org/data/2.5/';

/**
 * Manage the request to OpenWeatherMap api service.
 * @param  {Number} lan Langitude
 * @param  {Number} lon Longitude
 * @param  {String} type weather or forecast
 * @param  {String} lang language
 * @return {Promise} The fetch request promise
 */
function openWeatherMapRequest(lat, lon, type, lang){
    return fetch(`${baseUrl}${type}?lat=${lat}&lon=${lon}&appid=${apiKey}&lang=${lang}&units=metric&cnt=16`)
}

export default openWeatherMapRequest;